<?php

namespace App\Http\Controllers;

use FFMpeg;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Filters\Video\VideoFilters;
use FFMpeg\Format\Video\X264;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Test extends Controller
{

    public function testfun(Request $request)
    {

        $file              = $request->file('video');
        $arrayFileName     = explode(".", $file->getClientOriginalName());
        $filename          = $file->getClientOriginalName();
        $storage_path_full = '/' . $filename;
        $localVideo        = Storage::disk('public')->put($storage_path_full, file_get_contents($file));

        $bitrate = [

        	'144' => [
        			'width' => 256,
        			'height' => 144, 
        			'bitrate' => 100
        	],

        	'240' => [
        		
        			'width' => 426,
        			'height' => 240, 
        			'bitrate' => 700
        	],

        	'360' => [
        		
        			'width' => 640,
        			'height' => 360 ,
        			'bitrate' => 900
        	]

        ];

        $videoData = $bitrate[$request->quality];
       $lowBitrateFormat = (new X264('libmp3lame', 'libx264'))->setKiloBitrate($videoData['bitrate']);
        
        try {

        FFMpeg::fromDisk('public')
                ->open($filename)
                ->addFilter(function (VideoFilters $filters) use ($videoData) {
                    $filters->resize(new \FFMpeg\Coordinate\Dimension($videoData['width'],$videoData['height']));
                })
                ->export()
                ->toDisk('public')
                ->inFormat($lowBitrateFormat)
                ->save('converted_'.$filename);
           $msg = 'converted successfully';     
        } catch (Exception $e) {
        	$msg = 'Pleae Try again or used different quality';
        }

        return redirect('/')->with('msg',$msg);
    }
}
