<!DOCTYPE html>
<html>
<body>
@if (session('msg'))
    <div class="alert alert-success">
        {{ session('msg') }}
    </div>
@endif
<form action="{{route('testfun')}}" method="post" enctype="multipart/form-data">
	{{csrf_field()}}
  Select image to upload:
  <input type="file" name="video" id="video"><br>
  select quality
  <select name="quality">
  	<option value="144">144 quality</option>
  	<option value="240">240 quality</option>
  	<option value="360">360 quality</option>
  </select>
  <input type="submit" value="convert" name="submit">
</form>

</body>
</html>